#ifndef __Zip__Zip__
#define __Zip__Zip__

//
//  Zip.h
//  Zip
//
//  Created by Nathan Wehr on 9/16/14.
//  Copyright (c) 2014 EvriChart, Inc. All rights reserved.
//

// C++
#include <string>
#include <sstream>
#include <exception>
#include <iostream>

// Boost
#include <boost/property_tree/ptree.hpp>

// Eloquent
#include "Eloquent/Extensions/Filters/Filter.h"

// Internal
#include "Base64Algorithm.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// Base64 : Filter
	///////////////////////////////////////////////////////////////////////////////
	class Base64 : public Filter {
	public:
		Base64( const boost::property_tree::ptree::value_type& i_Config );
		virtual ~Base64();
		
		virtual bool operator<<( std::string& io_Data );
		
	protected:
		boost::optional<bool> m_Encode;
		boost::optional<bool> m_Decode;
		
	};
	
	
}

#endif /* defined(__Zip__Zip__) */
