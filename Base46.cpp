//
//  Zip.cpp
//  Zip
//
//  Created by Nathan Wehr on 9/16/14.
//  Copyright (c) 2014 EvriChart, Inc. All rights reserved.
//

#include "Base64.h"

Eloquent::Base64::Base64( const boost::property_tree::ptree::value_type& i_Config )
: Filter( i_Config )
, m_Encode( m_Config.second.get_optional<bool>( "encode" ) )
, m_Decode( m_Config.second.get_optional<bool>( "decode" ) )
{}

Eloquent::Base64::~Base64() {}
	
bool Eloquent::Base64::operator<<( std::string& io_Data ) {
	if( m_Decode.is_initialized() && m_Decode.get() == true ) {
		io_Data = base64_decode( io_Data );
		return Continue( true );
	}
	
	io_Data = base64_encode( reinterpret_cast<const unsigned char*>( io_Data.data() ), io_Data.length() );
	return Continue( true );
	
}
