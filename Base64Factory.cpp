//
//  ZipFactory.cpp
//  Zip
//
//  Created by Nathan Wehr on 9/16/14.
//  Copyright (c) 2014 EvriChart, Inc. All rights reserved.
//

#include "Base64Factory.h"

Eloquent::Base64Factory::Base64Factory() : FilterFactory() {}
Eloquent::Base64Factory::~Base64Factory() {}
	
Eloquent::Filter* Eloquent::Base64Factory::New( const boost::property_tree::ptree::value_type& i_ConfigNode ) {
	return new Base64( i_ConfigNode );
}
