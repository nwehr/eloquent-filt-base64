#ifndef __Base64__Base64Algorithm__
#define __Base64__Base64Algorithm__

//
//  Base64Algorithm.h
//  Base64
//
//  Created by Nathan Wehr on 9/17/14.
//  Copyright (c) 2014 EvriChart, Inc. All rights reserved.
//


#include <string>
#include <iostream>

std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len); 
std::string base64_decode(std::string const& encoded_string);

#endif /* defined(__Base64__Base64Algorithm__) */
