//
//  ZipAttach.cpp
//  Zip
//
//  Created by Nathan Wehr on 9/16/14.
//  Copyright (c) 2014 EvriChart, Inc. All rights reserved.
//

#include "Base64Attach.h"
#include "Base64Factory.h"

extern "C" void* Attach(void) {
	return new Eloquent::Base64Factory();
}